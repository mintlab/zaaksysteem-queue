package Zaaksysteem::Queue::Watcher::StatsD;

use Moose::Role;

=head1 NAME

Zaaksysteem::Queue::Watcher::StatsD - StatsD Module for Zaaksysteem Queuerunner

=head1 SYNOPSIS

    ### Increment by one
    $self->statsd->increment('employee.login.failed', 1);

=head1 DESCRIPTION

Interface to statsd daemon

=head1 ATTRIBUTES

=head2 $self->statsd

Return value: $STATSD_OBJECT

    my $statsd_object = $self->statsd;

Returns the L<Zaaksysteem::Queue::Watcher::StatsD::Backend> object, which is documented below

=cut

has 'statsd' => (
    is      => 'ro',
    default => sub {
        my $self    = shift;


        return Zaaksysteem::Queue::Watcher::StatsD::Backend->new()
    }
);

=head2 request_start_time

Placeholder to save a [gettimeofday] arrayref

=cut

has 'request_start_time' => (
    is  => 'rw',
    isa => 'ArrayRef',
);


package Zaaksysteem::Queue::Watcher::StatsD::Backend;

use Net::Statsd;
use Time::HiRes qw/gettimeofday tv_interval/;

use Moose;

has 'prefix'    => (
    is      => 'ro',
    default => sub {
        my $self    = shift;

        return 'zaaksysteem.queue.statsd.';
    }
);

=head2 _prepare

Prepares the StatsD daemon configuration (host/port)

=cut

sub _prepare {
    my $self            = shift;

    $Net::Statsd::HOST = '127.0.0.1';
    $Net::Statsd::PORT = 8125;

    return 1;
}

=head1 BACKEND METHODS

=head2 $c->statsd->increment($STRING_PREFIX, $INTEGER)

Return value: $BOOL_SUCCESS

    $c->statsd->increment('employee.login.failed', 1);

Increments a statsd counter with one.

=cut

sub increment {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::increment($self->prefix . $metric, @_);
}

=head2 $c->statsd->timing($STRING_PREFIX, $TIME_MICROSECONDS)

Return value: $BOOL_SUCCESS

    $c->statsd->timing('request.time', '2500');

Increments a statsd counter with one.

=cut

sub timing {
    my $self            = shift;
    my $metric          = shift;

    return unless $self->_prepare;

    return Net::Statsd::timing($self->prefix . $metric, @_);
}

=head2 $c->statsd->start()

Return value: [gettimeofday]

    $t0 = $c->statsd->start()

    $c->statsd->end('request.timing', $t0);
    ### Calculates difference between t0 and 'end'


Will return a t0 for timing later on. Convenience method.

=cut

sub start {
    my $self            = shift;

    return [gettimeofday];
}

=head2 $c->statsd->end()

Return value: $BOOLEAN_SUCCESS

    $t0 = $c->statsd->start()

    $c->statsd->end('request.timing', $t0);
    ### Calculates difference between t0 and 'end'


When given a C<t0>, it will calculate the time between it and sends it to this metric

=cut

sub end {
    my $self            = shift;
    my $metric          = shift;
    my $t0              = shift;

    Moose->throw_error('Expected an array of "gettimeofday"') unless (ref $t0 eq 'ARRAY');

    my $time = $self->milliseconds($t0);

    return $self->timing($metric, $time);
}

=head2 $self->statsd->milliseconds

    $self->statsd->milliseconds($t0);

Returns the time in milliseconds

=cut

sub milliseconds {
    my $self            = shift;
    my $t0              = shift;

    return int(tv_interval ( $t0, [gettimeofday])*1000);
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
