package Zaaksysteem::Queue::Watcher::DB;

use Moose::Role;

use DBI;

=head1 NAME

Zaaksysteem::Queue::Watcher::DB - DBI role for the queue watcher

=head1 DESCRIPTION

This role, when applied, ads a L</db_handles> attribute to the class/instance.

=head1 DEPENDENCIES

=head2 log

The C<log> method is expected to return an object with the usual logging
methods.

=head2 all_instance_configs

This method is expected to return a list of hashrefs with customer.d config
data.

=cut

requires qw[
    all_instance_configs
    log
];

=head1 ATTRIBUTES

=head1 db_handles

A convenience method that holds a reference to an array of L<DBI::db>
instances.

The instances are built via the C<_build_db_handles> method, which can be
overridden in the consuming class. The build hashref is indexed on the
filehandle of the connected DBI socket.

=head3 Proxied methods

=over 4

=item get_dbh

Gets a database handle by filehandle.

=item set_dbh

Sets a database handle by filehandle.

=item dbhs

Returns the list of database handle instances.

=item dbh_sockets

Returns the list of database socket filehandles.

=back

=cut

has db_handles => (
    is => 'ro',
    isa => 'HashRef[DBI::db]',
    lazy => 1,
    traits => [qw[Hash]],
    builder => '_build_db_handles',
    clearer => 'clear_db_handles',
    handles => {
        get_dbh => 'get',
        set_dbh => 'set',
        dbhs => 'values',
        dbh_sockets => 'keys'
    }
); 

sub _build_db_handles {
    my $self = shift;

    my %handles;
    my %dsns;

    my $t0 = $self->statsd->start;

    for my $instance ($self->all_instance_configs) {
        my $connect_info = $instance->{ 'Model::DB' }{ connect_info };

        next if $dsns{ $connect_info->{ dsn } }++;

        $self->log->info(sprintf(
            'Listening for "zs_action_queued" event on dsn:%s',
            $connect_info->{ dsn }
        ));

        my $dbh = DBI->connect(
            $connect_info->{ dsn },
            $connect_info->{ user },
            $connect_info->{ password },
            {
                PrintError => 0,
                AutoInactiveDestroy => 1
            }
        );

        unless (defined $dbh) {
            $self->log->info(sprintf(
                'Connection to dsn:%s failed, skipping',
                $connect_info->{ dsn }
            ));

            next;
        }

        $dbh->do('LISTEN zs_action_queued');

        $handles{ $dbh->{ pg_socket } } = $dbh;
    }

    $self->statsd->end('restart.db.time', $t0);

    return \%handles;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
