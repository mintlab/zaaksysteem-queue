package Zaaksysteem::Queue::Watcher::Config;

use Moose::Role;

use Config::Any;

=head1 NAME

Zaaksysteem::Queue::Watcher::Config - Configuration role for the queue watcher

=head1 DESCRIPTION

This role, when applied, adds a L</instance_configs> attribute to the
class/instance.

=head1 DEPENDENCIES

=head2 customer_d_path

This method should return a string path wherein customer configuration files
can be found.

=cut

requires qw[customer_d_path];

=head1 ATTRIBUTES

=head2 instance_configs

A convenience attribute that holds a reference to an array of hashrefs.

The value is build via the C<_build_instance_configs> method, which can be
overridden in the consuming class.

=head3 Proxied methods

=over 4

=item all_instance_configs

Returns the array elements as a list

=back

=cut

has instance_configs => (
    is => 'ro',
    isa => 'ArrayRef[HashRef]',
    lazy => 1,
    traits => [qw[Array]],
    builder => '_build_instance_configs',
    handles => {
        all_instance_configs => 'elements'
    }
);

sub _build_instance_configs {
    my $self = shift;

    unless (-d $self->customer_d_path && -r $self->customer_d_path) {
        Moose->throw_error('customer.d path not a directory or not readable');
    }

    my @files = glob sprintf('%s/*.conf', $self->customer_d_path);

    my $config = Config::Any->load_files({
        files => \@files,
        use_ext => 1,
        flatten_to_hash => 1
    });

    # flatten the configs some more, round up all instances, regardless of
    # config file or root name
    return [ map { values %{ $_ } } values %{ $config } ];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
